link db: https://dbdiagram.io/d/6256f0282514c9790325d2ce

<img src="Dokumentasi/erd.png">

<h5><b>Dokumentasi Api</b></h5>

routing
<img src="Dokumentasi/routing.png">

add data
<img src="Dokumentasi/add.png">

delete data
<img src="Dokumentasi/delete.png">

update data
<img src="Dokumentasi/update.png">

filter data
<img src="Dokumentasi/filter.png">

database
<img src="Dokumentasi/database.png">
