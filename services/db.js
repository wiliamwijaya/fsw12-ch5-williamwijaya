// reference by fatwa
const { cars } = require("../models");

const findalldata = async () => {
  const data = await cars.findAll();
  console.log(data);
  return data;
};

const findiddata = async (id) => {
  const car = await cars.findOne({
    where: { id: id },
  });
  let newcar = JSON.stringify(car, null, 2);
  newcar = JSON.parse(newcar);
  return newcar;
};

const createdata = (data) => {
  cars.create({
    name: data.name,
    size: data.size,
    image: data.image,
    price: data.price,
  });
};

const deletedata = (id) => {
  cars.destroy({
    where: {
      id: id,
    },
  });
};

const updatedata = (data) => {
  let newsize = "";
  if (data.size == "pilih size") {
    newsize = data.oldsize;
  } else {
    newsize = data.size;
  }

  const query = {
    where: { id: data.id },
  };

  cars.update(
    {
      name: data.name,
      size: data.size,
      image: data.image,
      price: data.price,
    },
    query
  );
};

const filterdata = async (size) => {
  const data = await cars.findAll();
  const newcar = data.filter((row) => row.size == size);
  return newcar;
};

module.exports = {
  createdata,
  findalldata,
  deletedata,
  updatedata,
  findiddata,
  filterdata,
};
