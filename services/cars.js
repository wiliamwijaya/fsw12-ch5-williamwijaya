// reference by fatwa

const {
  createdata,
  findalldata,
  deletedata,
  findiddata,
  updatedata,
  filterdata,
} = require("./db");

const pagemain = (req, res) => {
  const cars = findalldata();
  cars.then(function (result) {
    res.render("index", {
      layout: "layouts/main",
      title: "admin page",
      cars: result,
      msg: req.flash("msg"),
    });
  });
};

const pageadd = (req, res) => {
  res.render("add", {
    layout: "layouts/main",
    title: "add data car",
  });
};

const pageupdate = (req, res) => {
  const car = findiddata(req.params.id);
  car.then(function (result) {
    res.render("update", {
      layout: "layouts/main",
      title: "Halaman edit data mobil",
      car: result,
    });
  });
};

const addData = (req, res) => {
  createdata(req.body);
  setTimeout(reload, 2000);
  function reload() {
    req.flash("msg", "data berhasil ditambah");
    res.status(201).redirect("/");
  }
};

const deleteData = (req, res) => {
  deletedata(req.params.id);
  setTimeout(reload, 2000);
  function reload() {
    req.flash("msg", "data berhasil ditambah");
    res.status(201).redirect("/");
  }
};

const updateData = (req, res) => {
  updatedata(req.body);
  setTimeout(reload, 2000);
  function reload() {
    req.flash("msg", "data berhasil ditambah");
    res.status(201).redirect("/");
  }
};

const filterData = (req, res) => {
  const cars = filterdata(req.params.size);
  cars.then(function (result) {
    res.render("index", {
      layout: "layouts/main",
      title: "admin page",
      cars: result,
      msg: req.flash("msg"),
    });
  });
};

module.exports = {
  pagemain,
  pageadd,
  pageupdate,
  addData,
  deleteData,
  filterData,
  updateData,
};
