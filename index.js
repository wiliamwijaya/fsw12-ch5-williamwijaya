const express = require("express");
const expressLayouts = require("express-ejs-layouts");
const session = require("express-session");
const cookieParser = require("cookie-parser");
const flash = require("connect-flash");
const body = require("body-parser");
const {
  pagemain,
  pageadd,
  pageupdate,
  deleteData,
  addData,
  updateData,
  filterData,
} = require("./services/cars");

const app = express();
app.use(body.json());
app.use(body.urlencoded({ extended: true }));
app.set("view engine", "ejs");
app.use(expressLayouts);
app.use(express.static("public"));
app.use(cookieParser("secret"));
// reference by fatwa
app.use(
  session({
    cookie: { maxAge: 6000 },
    secret: "secret",
    resave: true,
    saveUninitialized: true,
  })
);
app.use(flash());

app.get("/", pagemain);
app.get("/add", pageadd);
app.get("/update/:id", pageupdate);
app.post("/add", addData);
app.get("/delete/:id", deleteData);
app.post("/update", updateData);
app.get("/filter/:size", filterData);

app.listen(8000, function () {
  console.log(`Server nyala di http://localhost:8000`);
});
